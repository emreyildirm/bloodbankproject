using AutoMapper;
using BloodBank.App;
using BloodBank.BLL.Helper;
using BloodBank.BLL.IServices;
using BloodBank.BLL.Services;
using BloodBank.DAL;
using BloodBank.DAL.Repositories.IRepo;
using BloodBank.DAL.Repositories.Repo;
using BloodBank.DAL.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Windows.Forms;

namespace BloodBankApp.UI
{
    static class Program
    {

        public static IServiceProvider ServiceProvider { get; set; }
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            IServiceCollection serviceCollection = new ServiceCollection();

            Application.SetHighDpiMode(HighDpiMode.SystemAware);

            ConfigureServices(serviceCollection);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            using (ServiceProvider serviceProvider = serviceCollection.BuildServiceProvider())
            {
                var MainForm = serviceProvider.GetRequiredService<MainForm>();
                Application.Run(MainForm);
            }
            //Application.Run(new MainForm());
        }
        static void ConfigureServices(IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<IDonorService, DonorService>();
            serviceCollection.AddScoped<IBankOfBloodService, BankOfBloodService>();
            serviceCollection.AddScoped<IBloodTypeService, BloodTypeService>();
            serviceCollection.AddScoped<IPendingDonorService, PendingDonorService>();
            serviceCollection.AddScoped<IAdminService, AdminService>();
            serviceCollection.AddScoped<IRoleService, RoleService>();


            serviceCollection.AddDbContext<BloodBankContext>(options => {
                options.UseSqlServer("Server = KAMOHOBE\\SQLEXPRESS; Database = BloodBankDB; Trusted_Connection = True");
            });

            serviceCollection.AddScoped<IUnitOfWork, UnitOfWork>();

            serviceCollection.AddScoped<DbContext, BloodBankContext>();



            serviceCollection.AddScoped<IDonorRepository, DonorRepository>();
            serviceCollection.AddScoped<IBankOfBloodRepository, BankOfBloodRepository>();
            serviceCollection.AddScoped<IBloodTypeRepository, BloodTypeRepository>();
            serviceCollection.AddScoped<IRoleRepository, RoleRepository>();
            serviceCollection.AddScoped<IAdminRepository, AdminRepository>();
            serviceCollection.AddScoped<IPendingDonorRepository, PendingDonorRepository>();

            //serviceCollection.AddTransient(typeof(BloodBankContext), typeof(BloodBankContext));
            //serviceCollection.AddTransient(typeof(IDonorRepository), typeof(DonorRepository));
            //serviceCollection.AddTransient(typeof(IBankOfBloodRepository), typeof(BankOfBloodRepository));
            //serviceCollection.AddTransient(typeof(IBloodTypeRepository), typeof(BloodTypeRepository));

            serviceCollection.AddScoped<MainForm>();

            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });

            IMapper mapper = mappingConfig.CreateMapper();
            serviceCollection.AddSingleton(mapper);
        }
    }
}
