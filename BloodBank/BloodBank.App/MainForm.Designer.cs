﻿namespace BloodBank.App
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BloodBankStatus_grpbox = new System.Windows.Forms.GroupBox();
            this.MaximumAmountofBloodGroupCount_lbl = new System.Windows.Forms.Label();
            this.MinimumAmountofBloodGroup_lbl = new System.Windows.Forms.Label();
            this.MinimumAmountofBloodGroupCount_lbl = new System.Windows.Forms.Label();
            this.MaximumAmountofBloodGroup_lbl = new System.Windows.Forms.Label();
            this.DonorsMostlyBloodType_lbl = new System.Windows.Forms.Label();
            this.TotalDonorCount_lbl = new System.Windows.Forms.Label();
            this.DonorStatus_grpbox = new System.Windows.Forms.GroupBox();
            this.DonorCount_lbl = new System.Windows.Forms.Label();
            this.DonorsMostlyBloodTypeCount_lbl = new System.Windows.Forms.Label();
            this.btn_AddDonorPanel = new System.Windows.Forms.Button();
            this.lbl_MenuOptions = new System.Windows.Forms.Label();
            this.grpbx_menu = new System.Windows.Forms.GroupBox();
            this.btn_BloodBank = new System.Windows.Forms.Button();
            this.btn_donorlist = new System.Windows.Forms.Button();
            this.pendingdonorcount_lbl = new System.Windows.Forms.Label();
            this.totalpendingdonors_lbl = new System.Windows.Forms.Label();
            this.login_btn = new System.Windows.Forms.Button();
            this.header_lbl = new System.Windows.Forms.Label();
            this.BloodBankStatus_grpbox.SuspendLayout();
            this.DonorStatus_grpbox.SuspendLayout();
            this.grpbx_menu.SuspendLayout();
            this.SuspendLayout();
            // 
            // BloodBankStatus_grpbox
            // 
            this.BloodBankStatus_grpbox.Controls.Add(this.MaximumAmountofBloodGroupCount_lbl);
            this.BloodBankStatus_grpbox.Controls.Add(this.MinimumAmountofBloodGroup_lbl);
            this.BloodBankStatus_grpbox.Controls.Add(this.MinimumAmountofBloodGroupCount_lbl);
            this.BloodBankStatus_grpbox.Controls.Add(this.MaximumAmountofBloodGroup_lbl);
            this.BloodBankStatus_grpbox.Location = new System.Drawing.Point(601, 380);
            this.BloodBankStatus_grpbox.Name = "BloodBankStatus_grpbox";
            this.BloodBankStatus_grpbox.Size = new System.Drawing.Size(459, 234);
            this.BloodBankStatus_grpbox.TabIndex = 0;
            this.BloodBankStatus_grpbox.TabStop = false;
            this.BloodBankStatus_grpbox.Text = "Blood Bank Status";
            // 
            // MaximumAmountofBloodGroupCount_lbl
            // 
            this.MaximumAmountofBloodGroupCount_lbl.AutoSize = true;
            this.MaximumAmountofBloodGroupCount_lbl.Location = new System.Drawing.Point(343, 92);
            this.MaximumAmountofBloodGroupCount_lbl.Name = "MaximumAmountofBloodGroupCount_lbl";
            this.MaximumAmountofBloodGroupCount_lbl.Size = new System.Drawing.Size(0, 20);
            this.MaximumAmountofBloodGroupCount_lbl.TabIndex = 0;
            // 
            // MinimumAmountofBloodGroup_lbl
            // 
            this.MinimumAmountofBloodGroup_lbl.AutoSize = true;
            this.MinimumAmountofBloodGroup_lbl.Location = new System.Drawing.Point(40, 65);
            this.MinimumAmountofBloodGroup_lbl.Name = "MinimumAmountofBloodGroup_lbl";
            this.MinimumAmountofBloodGroup_lbl.Size = new System.Drawing.Size(236, 20);
            this.MinimumAmountofBloodGroup_lbl.TabIndex = 0;
            this.MinimumAmountofBloodGroup_lbl.Text = "Minimum Amount of Blood Group";
            this.MinimumAmountofBloodGroup_lbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // MinimumAmountofBloodGroupCount_lbl
            // 
            this.MinimumAmountofBloodGroupCount_lbl.AutoSize = true;
            this.MinimumAmountofBloodGroupCount_lbl.Location = new System.Drawing.Point(343, 62);
            this.MinimumAmountofBloodGroupCount_lbl.Name = "MinimumAmountofBloodGroupCount_lbl";
            this.MinimumAmountofBloodGroupCount_lbl.Size = new System.Drawing.Size(0, 20);
            this.MinimumAmountofBloodGroupCount_lbl.TabIndex = 0;
            // 
            // MaximumAmountofBloodGroup_lbl
            // 
            this.MaximumAmountofBloodGroup_lbl.AutoSize = true;
            this.MaximumAmountofBloodGroup_lbl.Location = new System.Drawing.Point(40, 95);
            this.MaximumAmountofBloodGroup_lbl.Name = "MaximumAmountofBloodGroup_lbl";
            this.MaximumAmountofBloodGroup_lbl.Size = new System.Drawing.Size(239, 20);
            this.MaximumAmountofBloodGroup_lbl.TabIndex = 0;
            this.MaximumAmountofBloodGroup_lbl.Text = "Maximum Amount of Blood Group";
            // 
            // DonorsMostlyBloodType_lbl
            // 
            this.DonorsMostlyBloodType_lbl.AutoSize = true;
            this.DonorsMostlyBloodType_lbl.Location = new System.Drawing.Point(24, 95);
            this.DonorsMostlyBloodType_lbl.Name = "DonorsMostlyBloodType_lbl";
            this.DonorsMostlyBloodType_lbl.Size = new System.Drawing.Size(180, 20);
            this.DonorsMostlyBloodType_lbl.TabIndex = 0;
            this.DonorsMostlyBloodType_lbl.Text = "Donors Mostly BloodType";
            // 
            // TotalDonorCount_lbl
            // 
            this.TotalDonorCount_lbl.AutoSize = true;
            this.TotalDonorCount_lbl.Location = new System.Drawing.Point(24, 65);
            this.TotalDonorCount_lbl.Name = "TotalDonorCount_lbl";
            this.TotalDonorCount_lbl.Size = new System.Drawing.Size(131, 20);
            this.TotalDonorCount_lbl.TabIndex = 0;
            this.TotalDonorCount_lbl.Text = "Total Donor Count";
            // 
            // DonorStatus_grpbox
            // 
            this.DonorStatus_grpbox.Controls.Add(this.totalpendingdonors_lbl);
            this.DonorStatus_grpbox.Controls.Add(this.pendingdonorcount_lbl);
            this.DonorStatus_grpbox.Controls.Add(this.DonorCount_lbl);
            this.DonorStatus_grpbox.Controls.Add(this.DonorsMostlyBloodTypeCount_lbl);
            this.DonorStatus_grpbox.Controls.Add(this.TotalDonorCount_lbl);
            this.DonorStatus_grpbox.Controls.Add(this.DonorsMostlyBloodType_lbl);
            this.DonorStatus_grpbox.Location = new System.Drawing.Point(601, 159);
            this.DonorStatus_grpbox.Name = "DonorStatus_grpbox";
            this.DonorStatus_grpbox.Size = new System.Drawing.Size(459, 210);
            this.DonorStatus_grpbox.TabIndex = 1;
            this.DonorStatus_grpbox.TabStop = false;
            this.DonorStatus_grpbox.Text = "Donor Status";
            // 
            // DonorCount_lbl
            // 
            this.DonorCount_lbl.AutoSize = true;
            this.DonorCount_lbl.Location = new System.Drawing.Point(238, 65);
            this.DonorCount_lbl.Name = "DonorCount_lbl";
            this.DonorCount_lbl.Size = new System.Drawing.Size(0, 20);
            this.DonorCount_lbl.TabIndex = 0;
            // 
            // DonorsMostlyBloodTypeCount_lbl
            // 
            this.DonorsMostlyBloodTypeCount_lbl.AutoSize = true;
            this.DonorsMostlyBloodTypeCount_lbl.Location = new System.Drawing.Point(238, 95);
            this.DonorsMostlyBloodTypeCount_lbl.Name = "DonorsMostlyBloodTypeCount_lbl";
            this.DonorsMostlyBloodTypeCount_lbl.Size = new System.Drawing.Size(0, 20);
            this.DonorsMostlyBloodTypeCount_lbl.TabIndex = 0;
            // 
            // btn_AddDonorPanel
            // 
            this.btn_AddDonorPanel.Location = new System.Drawing.Point(142, 75);
            this.btn_AddDonorPanel.Name = "btn_AddDonorPanel";
            this.btn_AddDonorPanel.Size = new System.Drawing.Size(162, 69);
            this.btn_AddDonorPanel.TabIndex = 2;
            this.btn_AddDonorPanel.Text = "Add Donor";
            this.btn_AddDonorPanel.UseVisualStyleBackColor = true;
            this.btn_AddDonorPanel.Click += new System.EventHandler(this.btn_AddDonorPanel_Click);
            // 
            // lbl_MenuOptions
            // 
            this.lbl_MenuOptions.AutoSize = true;
            this.lbl_MenuOptions.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbl_MenuOptions.Location = new System.Drawing.Point(173, 23);
            this.lbl_MenuOptions.Name = "lbl_MenuOptions";
            this.lbl_MenuOptions.Size = new System.Drawing.Size(78, 35);
            this.lbl_MenuOptions.TabIndex = 3;
            this.lbl_MenuOptions.Text = "Menu";
            this.lbl_MenuOptions.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // grpbx_menu
            // 
            this.grpbx_menu.Controls.Add(this.btn_donorlist);
            this.grpbx_menu.Controls.Add(this.btn_BloodBank);
            this.grpbx_menu.Controls.Add(this.btn_AddDonorPanel);
            this.grpbx_menu.Controls.Add(this.lbl_MenuOptions);
            this.grpbx_menu.Location = new System.Drawing.Point(35, 195);
            this.grpbx_menu.Name = "grpbx_menu";
            this.grpbx_menu.Size = new System.Drawing.Size(459, 419);
            this.grpbx_menu.TabIndex = 4;
            this.grpbx_menu.TabStop = false;
            // 
            // btn_BloodBank
            // 
            this.btn_BloodBank.Location = new System.Drawing.Point(142, 170);
            this.btn_BloodBank.Name = "btn_BloodBank";
            this.btn_BloodBank.Size = new System.Drawing.Size(162, 69);
            this.btn_BloodBank.TabIndex = 2;
            this.btn_BloodBank.Text = "Blood Bank";
            this.btn_BloodBank.UseVisualStyleBackColor = true;
            this.btn_BloodBank.Click += new System.EventHandler(this.btn_BloodBank_Click);
            // 
            // btn_donorlist
            // 
            this.btn_donorlist.Location = new System.Drawing.Point(142, 278);
            this.btn_donorlist.Name = "btn_donorlist";
            this.btn_donorlist.Size = new System.Drawing.Size(162, 69);
            this.btn_donorlist.TabIndex = 2;
            this.btn_donorlist.Text = "Donor List";
            this.btn_donorlist.UseVisualStyleBackColor = true;
            this.btn_donorlist.Click += new System.EventHandler(this.btn_donorlist_Click);
            // 
            // pendingdonorcount_lbl
            // 
            this.pendingdonorcount_lbl.AutoSize = true;
            this.pendingdonorcount_lbl.Location = new System.Drawing.Point(238, 140);
            this.pendingdonorcount_lbl.Name = "pendingdonorcount_lbl";
            this.pendingdonorcount_lbl.Size = new System.Drawing.Size(0, 20);
            this.pendingdonorcount_lbl.TabIndex = 1;
            // 
            // totalpendingdonors_lbl
            // 
            this.totalpendingdonors_lbl.AutoSize = true;
            this.totalpendingdonors_lbl.Location = new System.Drawing.Point(24, 140);
            this.totalpendingdonors_lbl.Name = "totalpendingdonors_lbl";
            this.totalpendingdonors_lbl.Size = new System.Drawing.Size(151, 20);
            this.totalpendingdonors_lbl.TabIndex = 1;
            this.totalpendingdonors_lbl.Text = "Pending Donor Count";
            // 
            // login_btn
            // 
            this.login_btn.Location = new System.Drawing.Point(35, 48);
            this.login_btn.Name = "login_btn";
            this.login_btn.Size = new System.Drawing.Size(118, 38);
            this.login_btn.TabIndex = 3;
            this.login_btn.Text = "Login";
            this.login_btn.UseVisualStyleBackColor = true;
            this.login_btn.Click += new System.EventHandler(this.login_btn_Click);
            // 
            // header_lbl
            // 
            this.header_lbl.AutoSize = true;
            this.header_lbl.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.header_lbl.Font = new System.Drawing.Font("Lucida Sans Unicode", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point);
            this.header_lbl.Location = new System.Drawing.Point(401, 49);
            this.header_lbl.Name = "header_lbl";
            this.header_lbl.Size = new System.Drawing.Size(260, 37);
            this.header_lbl.TabIndex = 6;
            this.header_lbl.Text = "Blood Bank App";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1083, 647);
            this.Controls.Add(this.header_lbl);
            this.Controls.Add(this.login_btn);
            this.Controls.Add(this.grpbx_menu);
            this.Controls.Add(this.DonorStatus_grpbox);
            this.Controls.Add(this.BloodBankStatus_grpbox);
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.BloodBankStatus_grpbox.ResumeLayout(false);
            this.BloodBankStatus_grpbox.PerformLayout();
            this.DonorStatus_grpbox.ResumeLayout(false);
            this.DonorStatus_grpbox.PerformLayout();
            this.grpbx_menu.ResumeLayout(false);
            this.grpbx_menu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox BloodBankStatus_grpbox;
        private System.Windows.Forms.GroupBox DonorStatus_grpbox;
        private System.Windows.Forms.Label DonorsMostlyBloodType_lbl;
        private System.Windows.Forms.Label TotalDonorCount_lbl;
        private System.Windows.Forms.Label MaximumAmountofBloodGroupCount_lbl;
        private System.Windows.Forms.Label MinimumAmountofBloodGroup_lbl;
        private System.Windows.Forms.Label MinimumAmountofBloodGroupCount_lbl;
        private System.Windows.Forms.Label MaximumAmountofBloodGroup_lbl;
        private System.Windows.Forms.Label DonorCount_lbl;
        private System.Windows.Forms.Label DonorsMostlyBloodTypeCount_lbl;
        private System.Windows.Forms.Button btn_DonorMenu;
        private System.Windows.Forms.Label lbl_MenuOptions;
        private System.Windows.Forms.Button btn_AddDonorMenu;
        private System.Windows.Forms.GroupBox grpbx_menu;
        private System.Windows.Forms.Button btn_AddDonorPanel;
        private System.Windows.Forms.Button btn_BloodBank;
        private System.Windows.Forms.Button btn_donorlist;
        private System.Windows.Forms.Label totalpendingdonors_lbl;
        private System.Windows.Forms.Label pendingdonorcount_lbl;
        private System.Windows.Forms.Button login_btn;
        private System.Windows.Forms.Label header_lbl;
    }
}