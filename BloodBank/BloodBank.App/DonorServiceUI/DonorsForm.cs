﻿using BloodBank.BLL.DTO;
using BloodBank.BLL.IServices;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace BloodBank.App.DonorServiceUI
{
    public partial class DonorsForm : Form
    {
        private IPendingDonorService pendingDonorService;
        private List<BloodTypeDto> bloodTypes;
        private List<PendingDonorDto> donors;
        public DonorsForm(IPendingDonorService pendingDonorService, List<BloodTypeDto> bloodTypes)
        {
            this.pendingDonorService = pendingDonorService;
            this.bloodTypes = bloodTypes;
            InitializeComponent();
        }

        private void DonorsForm_Load(object sender, EventArgs e)
        {
            RefreshList();
            FillComboBox();
        }

        private void RefreshList()
        {
            dataGridView_Donors.DataSource = null;
            donors = pendingDonorService.GetAll();
            dataGridView_Donors.DataSource = donors;
        }

        private void FillComboBox()
        {
            cmbbox_bloodtypes.DataSource = bloodTypes;
        }

        private void SearchDonorWithBloodType()
        {
            var selectedBloodType = this.cmbbox_bloodtypes.GetItemText(this.cmbbox_bloodtypes.SelectedItem);
            var findingDonors = pendingDonorService.FindWithBloodType(selectedBloodType);
            if (findingDonors.Count != 0)
            {
                dataGridView_Donors.DataSource = null;
                dataGridView_Donors.DataSource = findingDonors;
            }
            else
            {
                MessageBox.Show("Not Found");
            }

        }

        private void btn_search_Click(object sender, EventArgs e)
        {
            if (cmbbox_bloodtypes.SelectedItem == null && txt_lastname.Text == null && txt_idenditynumber.Text == null && txtbx_firstame.Text == null)
            {
                MessageBox.Show("Please Fill In One Of The Fields. Or Select Blood Type");
            }
            else if (cmbbox_bloodtypes.SelectedItem != null && txt_lastname.Text == null && txt_idenditynumber.Text == null && txtbx_firstame.Text == null)
            {
                SearchDonorWithBloodType();
            }
            else if (cmbbox_bloodtypes.SelectedItem == null && txt_lastname.Text != null && txt_idenditynumber.Text == null && txtbx_firstame.Text != null)
            {
                SearchDonorWithFullName();
            }
            else if (cmbbox_bloodtypes.SelectedItem == null && txt_lastname.Text == null && txt_idenditynumber.Text != null && txtbx_firstame.Text == null)
            {
                SearchWithIdendityNumber();
            }
        }

        private void SearchWithIdendityNumber()
        {
            var idendityNumber = txt_idenditynumber.Text;
            var findingDonor = pendingDonorService.FindWithIdendityNumber(idendityNumber);
            if (findingDonor.Count != 0)
            {
                dataGridView_Donors.DataSource = null;
                dataGridView_Donors.DataSource = findingDonor;
            }
            else
            {
                MessageBox.Show("Not Found");
            }
        }

        private void SearchDonorWithFullName()
        {
            var firstName = txtbx_firstame.Text;
            var lastName = txt_lastname.Text;
            var findingDonors = pendingDonorService.FindWithFullName(firstName, lastName);
            if (findingDonors.Count != 0)
            {
                dataGridView_Donors.DataSource = null;
                dataGridView_Donors.DataSource = findingDonors;
            }
            else
            {
                MessageBox.Show("Not Found");
            }
        }
    }
}
