﻿namespace BloodBank.App.DonorServiceUI
{
    partial class DonorsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridView_Donors = new System.Windows.Forms.DataGridView();
            this.cmbbox_bloodtypes = new System.Windows.Forms.ComboBox();
            this.txtbx_firstame = new System.Windows.Forms.TextBox();
            this.txt_lastname = new System.Windows.Forms.TextBox();
            this.btn_search = new System.Windows.Forms.Button();
            this.txt_idenditynumber = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Donors)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(383, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(154, 41);
            this.label1.TabIndex = 1;
            this.label1.Text = "Donor List";
            // 
            // dataGridView_Donors
            // 
            this.dataGridView_Donors.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_Donors.Location = new System.Drawing.Point(26, 130);
            this.dataGridView_Donors.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.dataGridView_Donors.Name = "dataGridView_Donors";
            this.dataGridView_Donors.RowHeadersWidth = 51;
            this.dataGridView_Donors.Size = new System.Drawing.Size(800, 400);
            this.dataGridView_Donors.TabIndex = 0;
            // 
            // cmbbox_bloodtypes
            // 
            this.cmbbox_bloodtypes.FormattingEnabled = true;
            this.cmbbox_bloodtypes.Location = new System.Drawing.Point(997, 130);
            this.cmbbox_bloodtypes.Name = "cmbbox_bloodtypes";
            this.cmbbox_bloodtypes.Size = new System.Drawing.Size(151, 28);
            this.cmbbox_bloodtypes.TabIndex = 2;
            // 
            // txtbx_firstame
            // 
            this.txtbx_firstame.Location = new System.Drawing.Point(997, 210);
            this.txtbx_firstame.Name = "txtbx_firstame";
            this.txtbx_firstame.Size = new System.Drawing.Size(151, 27);
            this.txtbx_firstame.TabIndex = 3;
            // 
            // txt_lastname
            // 
            this.txt_lastname.Location = new System.Drawing.Point(997, 275);
            this.txt_lastname.Name = "txt_lastname";
            this.txt_lastname.Size = new System.Drawing.Size(151, 27);
            this.txt_lastname.TabIndex = 4;
            // 
            // btn_search
            // 
            this.btn_search.Location = new System.Drawing.Point(1013, 410);
            this.btn_search.Name = "btn_search";
            this.btn_search.Size = new System.Drawing.Size(117, 44);
            this.btn_search.TabIndex = 5;
            this.btn_search.Text = "Search";
            this.btn_search.UseVisualStyleBackColor = true;
            this.btn_search.Click += new System.EventHandler(this.btn_search_Click);
            // 
            // txt_idenditynumber
            // 
            this.txt_idenditynumber.Location = new System.Drawing.Point(997, 343);
            this.txt_idenditynumber.Name = "txt_idenditynumber";
            this.txt_idenditynumber.Size = new System.Drawing.Size(151, 27);
            this.txt_idenditynumber.TabIndex = 6;
            // 
            // DonorsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1257, 638);
            this.Controls.Add(this.txt_idenditynumber);
            this.Controls.Add(this.btn_search);
            this.Controls.Add(this.txt_lastname);
            this.Controls.Add(this.txtbx_firstame);
            this.Controls.Add(this.cmbbox_bloodtypes);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridView_Donors);
            this.Name = "DonorsForm";
            this.Text = "DonorsForm";
            this.Load += new System.EventHandler(this.DonorsForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Donors)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridView_Donors;
        private System.Windows.Forms.ComboBox cmbbox_bloodtypes;
        private System.Windows.Forms.TextBox txtbx_firstame;
        private System.Windows.Forms.TextBox txt_lastname;
        private System.Windows.Forms.Button btn_search;
        private System.Windows.Forms.TextBox txt_idenditynumber;
    }
}