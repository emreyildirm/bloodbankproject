﻿using BloodBank.BLL.DTO;
using BloodBank.BLL.IServices;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace BloodBank.App.DonorServiceUI
{

    public partial class AddDonorForm : Form
    {
        private readonly IPendingDonorService pendingDonorService;
        private List<BloodTypeDto> bloodTypes;

        public AddDonorForm(IPendingDonorService pendingDonorService, List<BloodTypeDto> bloodTypes)
        {
            this.pendingDonorService = pendingDonorService;
            this.bloodTypes = bloodTypes;
            InitializeComponent();
        }

        private void AddDonorForm_Load(object sender, EventArgs e)
        {
            FillComboBox();
        }

        private void FillComboBox()
        {
            cmbbx_bloodtype.DataSource = bloodTypes;
            cmbbx_bloodtype.DisplayMember = "BloodTypeName";
            cmbbx_bloodtype.ValueMember = "Id";
        }

        private void btn_adddonor_Click(object sender, EventArgs e)
        {
            CreateDonor();
        }
        private void CreateDonor()
        {
            try
            {
                var pendingDonorDto = new PendingDonorDto()
                {
                    FirstName = txt_firstname.Text,
                    LastName = txt_lastname.Text,
                    Adress = txt_adress.Text,
                    DateOfBirth = date_birthday.Value,
                    BloodQuantity = 2,
                    BloodTypeId = Convert.ToInt32(cmbbx_bloodtype.SelectedValue.ToString()),
                    BloodTypeName = this.cmbbx_bloodtype.GetItemText(this.cmbbx_bloodtype.SelectedItem),
                    PhoneNumber = txt_phonenumber.Text,
                    Id = GetId()
                };
                pendingDonorService.Add(pendingDonorDto);
                MessageBox.Show("It was created successfully.");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }
        private int GetId()
        {
            return pendingDonorService.IdGenerator();
        }
    }
}
