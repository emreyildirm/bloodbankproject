﻿namespace BloodBank.App.DonorServiceUI
{
    partial class AddDonorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_donorinfo = new System.Windows.Forms.Label();
            this.txt_firstname = new System.Windows.Forms.TextBox();
            this.txt_lastname = new System.Windows.Forms.TextBox();
            this.txt_phonenumber = new System.Windows.Forms.TextBox();
            this.txt_adress = new System.Windows.Forms.TextBox();
            this.cmbbx_bloodtype = new System.Windows.Forms.ComboBox();
            this.date_birthday = new System.Windows.Forms.DateTimePicker();
            this.lbl_adress = new System.Windows.Forms.Label();
            this.lbl_dateofbirth = new System.Windows.Forms.Label();
            this.lbl_Phonenumber = new System.Windows.Forms.Label();
            this.lbl_LastName = new System.Windows.Forms.Label();
            this.lbl_FirstName = new System.Windows.Forms.Label();
            this.lbl_BloodType = new System.Windows.Forms.Label();
            this.btn_adddonor = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbl_donorinfo
            // 
            this.lbl_donorinfo.AutoSize = true;
            this.lbl_donorinfo.Font = new System.Drawing.Font("Segoe UI", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbl_donorinfo.Location = new System.Drawing.Point(305, 9);
            this.lbl_donorinfo.Name = "lbl_donorinfo";
            this.lbl_donorinfo.Size = new System.Drawing.Size(300, 46);
            this.lbl_donorinfo.TabIndex = 0;
            this.lbl_donorinfo.Text = "Donor Information";
            // 
            // txt_firstname
            // 
            this.txt_firstname.Location = new System.Drawing.Point(184, 81);
            this.txt_firstname.Name = "txt_firstname";
            this.txt_firstname.Size = new System.Drawing.Size(161, 27);
            this.txt_firstname.TabIndex = 1;
            // 
            // txt_lastname
            // 
            this.txt_lastname.Location = new System.Drawing.Point(184, 144);
            this.txt_lastname.Name = "txt_lastname";
            this.txt_lastname.Size = new System.Drawing.Size(161, 27);
            this.txt_lastname.TabIndex = 2;
            // 
            // txt_phonenumber
            // 
            this.txt_phonenumber.Location = new System.Drawing.Point(184, 207);
            this.txt_phonenumber.MaxLength = 10;
            this.txt_phonenumber.Name = "txt_phonenumber";
            this.txt_phonenumber.Size = new System.Drawing.Size(161, 27);
            this.txt_phonenumber.TabIndex = 3;
            // 
            // txt_adress
            // 
            this.txt_adress.Location = new System.Drawing.Point(184, 348);
            this.txt_adress.Multiline = true;
            this.txt_adress.Name = "txt_adress";
            this.txt_adress.Size = new System.Drawing.Size(271, 134);
            this.txt_adress.TabIndex = 5;
            // 
            // cmbbx_bloodtype
            // 
            this.cmbbx_bloodtype.FormattingEnabled = true;
            this.cmbbx_bloodtype.Location = new System.Drawing.Point(652, 105);
            this.cmbbx_bloodtype.Name = "cmbbx_bloodtype";
            this.cmbbx_bloodtype.Size = new System.Drawing.Size(151, 28);
            this.cmbbx_bloodtype.TabIndex = 6;
            // 
            // date_birthday
            // 
            this.date_birthday.Location = new System.Drawing.Point(184, 279);
            this.date_birthday.Name = "date_birthday";
            this.date_birthday.Size = new System.Drawing.Size(250, 27);
            this.date_birthday.TabIndex = 7;
            // 
            // lbl_adress
            // 
            this.lbl_adress.AutoSize = true;
            this.lbl_adress.Location = new System.Drawing.Point(94, 348);
            this.lbl_adress.Name = "lbl_adress";
            this.lbl_adress.Size = new System.Drawing.Size(69, 20);
            this.lbl_adress.TabIndex = 8;
            this.lbl_adress.Text = "Address :";
            // 
            // lbl_dateofbirth
            // 
            this.lbl_dateofbirth.AutoSize = true;
            this.lbl_dateofbirth.Location = new System.Drawing.Point(61, 284);
            this.lbl_dateofbirth.Name = "lbl_dateofbirth";
            this.lbl_dateofbirth.Size = new System.Drawing.Size(101, 20);
            this.lbl_dateofbirth.TabIndex = 9;
            this.lbl_dateofbirth.Text = "Date of Birth :";
            // 
            // lbl_Phonenumber
            // 
            this.lbl_Phonenumber.AutoSize = true;
            this.lbl_Phonenumber.Location = new System.Drawing.Point(61, 210);
            this.lbl_Phonenumber.Name = "lbl_Phonenumber";
            this.lbl_Phonenumber.Size = new System.Drawing.Size(115, 20);
            this.lbl_Phonenumber.TabIndex = 9;
            this.lbl_Phonenumber.Text = "Phone Number :";
            // 
            // lbl_LastName
            // 
            this.lbl_LastName.AutoSize = true;
            this.lbl_LastName.Location = new System.Drawing.Point(94, 151);
            this.lbl_LastName.Name = "lbl_LastName";
            this.lbl_LastName.Size = new System.Drawing.Size(82, 20);
            this.lbl_LastName.TabIndex = 9;
            this.lbl_LastName.Text = "LastName :";
            // 
            // lbl_FirstName
            // 
            this.lbl_FirstName.AutoSize = true;
            this.lbl_FirstName.Location = new System.Drawing.Point(94, 84);
            this.lbl_FirstName.Name = "lbl_FirstName";
            this.lbl_FirstName.Size = new System.Drawing.Size(83, 20);
            this.lbl_FirstName.TabIndex = 9;
            this.lbl_FirstName.Text = "FirstName :";
            // 
            // lbl_BloodType
            // 
            this.lbl_BloodType.AutoSize = true;
            this.lbl_BloodType.Location = new System.Drawing.Point(541, 108);
            this.lbl_BloodType.Name = "lbl_BloodType";
            this.lbl_BloodType.Size = new System.Drawing.Size(91, 20);
            this.lbl_BloodType.TabIndex = 10;
            this.lbl_BloodType.Text = "Blood Type :";
            // 
            // btn_adddonor
            // 
            this.btn_adddonor.Location = new System.Drawing.Point(629, 348);
            this.btn_adddonor.Name = "btn_adddonor";
            this.btn_adddonor.Size = new System.Drawing.Size(144, 46);
            this.btn_adddonor.TabIndex = 11;
            this.btn_adddonor.Text = "Add Donor";
            this.btn_adddonor.UseVisualStyleBackColor = true;
            this.btn_adddonor.Click += new System.EventHandler(this.btn_adddonor_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(184, 241);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(291, 20);
            this.label1.TabIndex = 12;
            this.label1.Text = "* Write your number without the leading 0.";
            // 
            // AddDonorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(931, 532);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_adddonor);
            this.Controls.Add(this.lbl_BloodType);
            this.Controls.Add(this.lbl_FirstName);
            this.Controls.Add(this.lbl_LastName);
            this.Controls.Add(this.lbl_Phonenumber);
            this.Controls.Add(this.lbl_dateofbirth);
            this.Controls.Add(this.lbl_adress);
            this.Controls.Add(this.date_birthday);
            this.Controls.Add(this.cmbbx_bloodtype);
            this.Controls.Add(this.txt_adress);
            this.Controls.Add(this.txt_phonenumber);
            this.Controls.Add(this.txt_lastname);
            this.Controls.Add(this.txt_firstname);
            this.Controls.Add(this.lbl_donorinfo);
            this.Name = "AddDonorForm";
            this.Text = "AddDonorForm";
            this.Load += new System.EventHandler(this.AddDonorForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_donorinfo;
        private System.Windows.Forms.TextBox txt_firstname;
        private System.Windows.Forms.TextBox txt_lastname;
        private System.Windows.Forms.TextBox txt_phonenumber;
        private System.Windows.Forms.TextBox txt_adress;
        private System.Windows.Forms.ComboBox cmbbx_bloodtype;
        private System.Windows.Forms.DateTimePicker date_birthday;
        private System.Windows.Forms.Label lbl_adress;
        private System.Windows.Forms.Label lbl_dateofbirth;
        private System.Windows.Forms.Label lbl_Phonenumber;
        private System.Windows.Forms.Label lbl_LastName;
        private System.Windows.Forms.Label lbl_FirstName;
        private System.Windows.Forms.Label lbl_BloodType;
        private System.Windows.Forms.Button btn_adddonor;
        private System.Windows.Forms.Label label1;
    }
}