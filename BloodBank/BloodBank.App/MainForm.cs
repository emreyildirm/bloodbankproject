﻿using BloodBank.App.AdminUI;
using BloodBank.App.BankServiceUI;
using BloodBank.App.DonorServiceUI;
using BloodBank.BLL.DTO;
using BloodBank.BLL.IServices;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace BloodBank.App
{
    public partial class MainForm : Form
    {
        private readonly IBankOfBloodService bankOfBloodService;
        private readonly IDonorService donorService;
        private readonly IBloodTypeService bloodTypeService;
        private readonly IPendingDonorService pendingDonorService;
        private readonly IAdminService adminService;

        private List<BloodTypeDto> bloodTypes;
        public MainForm(IPendingDonorService pendingDonorService, IAdminService adminService, IDonorService donorService, IBankOfBloodService bankOfBloodService, IBloodTypeService bloodTypeService)
        {
            this.donorService = donorService;
            this.adminService = adminService;
            this.pendingDonorService = pendingDonorService;
            this.bankOfBloodService = bankOfBloodService;
            this.bloodTypeService = bloodTypeService;
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            GetBloodTypes();
            FillDashBoard();
        }

        private void GetBloodTypes()
        {
            bloodTypes = bloodTypeService.GetAll();
        }

        private void FillDashBoard()
        {
            DonorCount_lbl.Text = donorService.GetDonorCount().ToString();
            //DonorsMostlyBloodTypeCount_lbl.Text = donorService.GetDonorsMostlyBloodType();
            MaximumAmountofBloodGroupCount_lbl.Text = bankOfBloodService.GetMaxQuantityByBloodType().ToString();
            MinimumAmountofBloodGroupCount_lbl.Text = bankOfBloodService.GetMinQuantityByBloodType().ToString();
            //pendingdonorcount_lbl.Text = pendingDonorService.GetPendingDonorCount().ToString();
        }
        private void OpenAddPendingDonorForm()
        {
            AddDonorForm addDonorForm = new AddDonorForm(pendingDonorService, bloodTypes);
            addDonorForm.Show();
            //this.Enabled = false;
        }

        private void OpenFindDonorForm()
        {
            DonorsForm donorsForm = new DonorsForm(pendingDonorService, bloodTypes);
            donorsForm.Show();
            //this.Enabled = false;
        }

        private void OpenBloodBankForm()
        {
            BloodBankStatusForm bloodBankForm = new BloodBankStatusForm(bankOfBloodService);
            bloodBankForm.Show();
            //this.Enabled  = false;
        }

        private void btn_AddDonorPanel_Click(object sender, EventArgs e)
        {
            OpenAddPendingDonorForm();
        }

        private void OpenLoginForm()
        {
            LoginForm loginForm = new LoginForm(adminService, pendingDonorService, donorService, bankOfBloodService, bloodTypes);
            loginForm.Show();
        }
        
        private void btn_donorlist_Click(object sender, EventArgs e)
        {
            OpenFindDonorForm();
        }

        private void btn_BloodBank_Click(object sender, EventArgs e)
        {
            OpenBloodBankForm();
        }

        private void login_btn_Click(object sender, EventArgs e)
        {
            OpenLoginForm();
        }
    }
}
