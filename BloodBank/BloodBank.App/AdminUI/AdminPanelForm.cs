﻿using BloodBank.BLL.DTO;
using BloodBank.BLL.IServices;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace BloodBank.App.AdminUI
{
    
    public partial class AdminPanelForm : Form
    {
        private AdminDto admin;
        private readonly IAdminService adminService;
        private readonly IBankOfBloodService bankOfBloodService;
        private readonly IDonorService donorService;
        private readonly IPendingDonorService pendingDonorService;
        private List<BloodTypeDto> bloodTypes;
        public AdminPanelForm(AdminDto admin, IAdminService adminService, IBankOfBloodService bankOfBloodService, IDonorService donorService, IPendingDonorService pendingDonorService, List<BloodTypeDto> bloodTypes)
        {
            this.admin = admin;
            this.adminService = adminService;
            this.bankOfBloodService = bankOfBloodService;
            this.donorService = donorService;
            this.pendingDonorService = pendingDonorService;
            this.bloodTypes = bloodTypes;
            InitializeComponent();
        }

        private void AdminPanelForm_Load(object sender, EventArgs e)
        {
            username_lbl.Text = admin.Username;
            RefreshDataViews();
        }

        private void logout_btn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void RefreshDataViews()
        {
            dataGridView_PendingDonors.DataSource = null;
            dataGridView_Donors.DataSource = null;
            dataGridView_BloodBank.DataSource = null;
            dataGridView_BloodBank.DataSource = bankOfBloodService.GetAll();
            dataGridView_Donors.DataSource = donorService.GetAll();
            dataGridView_PendingDonors.DataSource = pendingDonorService.GetAll();
        }

        private void btn_confirm_Click(object sender, EventArgs e)
        {
            var processedObject = GetSelectedPendingDonor();
            if (processedObject == null)
            {
                MessageBox.Show("Please Select 1 Pending Donor.");
            }
            else
            {
                BankUpdate(processedObject);
                ConfirmedDonorAddDonorList(processedObject);
                ConfirmedDonorDelete(processedObject);
                RefreshDataViews();
            }
        }
        private void BankUpdate(PendingDonorDto donor)
        {
            var bank = bankOfBloodService.GetByBoodType(donor.BloodTypeName);
            bankOfBloodService.AddingBlood(bank);
        }
        private PendingDonorDto GetSelectedPendingDonor()
        {
            if (dataGridView_PendingDonors.SelectedRows.Count != 0)
            {
                DataGridViewRow row = this.dataGridView_PendingDonors.SelectedRows[0];
                var confirmDonorId = Convert.ToInt32(row.Cells["Id"].Value);
                return pendingDonorService.GetById(confirmDonorId);
            }
            else
                return null;
        }
        private void ConfirmedDonorDelete(PendingDonorDto pendingDonorDto)
        {
            pendingDonorService.Remove(pendingDonorDto);
        }
        private void ConfirmedDonorAddDonorList(PendingDonorDto pendingDonorDto)
        {
            donorService.ConfirmPendingDonor(pendingDonorDto);
        }
    }
}
