﻿namespace BloodBank.App.AdminUI
{
    partial class LoginForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.username_lbl = new System.Windows.Forms.Label();
            this.passwprd_lbl = new System.Windows.Forms.Label();
            this.txt_username = new System.Windows.Forms.TextBox();
            this.txt_password = new System.Windows.Forms.TextBox();
            this.header_lbl = new System.Windows.Forms.Label();
            this.login_btn = new System.Windows.Forms.Button();
            this.unmask_chkbx = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // username_lbl
            // 
            this.username_lbl.AutoSize = true;
            this.username_lbl.Location = new System.Drawing.Point(106, 135);
            this.username_lbl.Name = "username_lbl";
            this.username_lbl.Size = new System.Drawing.Size(75, 20);
            this.username_lbl.TabIndex = 0;
            this.username_lbl.Text = "Username";
            // 
            // passwprd_lbl
            // 
            this.passwprd_lbl.AutoSize = true;
            this.passwprd_lbl.Location = new System.Drawing.Point(106, 192);
            this.passwprd_lbl.Name = "passwprd_lbl";
            this.passwprd_lbl.Size = new System.Drawing.Size(70, 20);
            this.passwprd_lbl.TabIndex = 1;
            this.passwprd_lbl.Text = "Password";
            // 
            // txt_username
            // 
            this.txt_username.Location = new System.Drawing.Point(225, 128);
            this.txt_username.Name = "txt_username";
            this.txt_username.Size = new System.Drawing.Size(146, 27);
            this.txt_username.TabIndex = 2;
            // 
            // txt_password
            // 
            this.txt_password.Location = new System.Drawing.Point(225, 192);
            this.txt_password.Name = "txt_password";
            this.txt_password.Size = new System.Drawing.Size(146, 27);
            this.txt_password.TabIndex = 3;
            // 
            // header_lbl
            // 
            this.header_lbl.AutoSize = true;
            this.header_lbl.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.header_lbl.Location = new System.Drawing.Point(228, 48);
            this.header_lbl.Name = "header_lbl";
            this.header_lbl.Size = new System.Drawing.Size(143, 35);
            this.header_lbl.TabIndex = 4;
            this.header_lbl.Text = "Login Panel";
            // 
            // login_btn
            // 
            this.login_btn.Location = new System.Drawing.Point(239, 269);
            this.login_btn.Name = "login_btn";
            this.login_btn.Size = new System.Drawing.Size(94, 29);
            this.login_btn.TabIndex = 5;
            this.login_btn.Text = "Login";
            this.login_btn.UseVisualStyleBackColor = true;
            this.login_btn.Click += new System.EventHandler(this.login_btn_Click);
            // 
            // unmask_chkbx
            // 
            this.unmask_chkbx.AutoSize = true;
            this.unmask_chkbx.Location = new System.Drawing.Point(401, 195);
            this.unmask_chkbx.Name = "unmask_chkbx";
            this.unmask_chkbx.Size = new System.Drawing.Size(83, 24);
            this.unmask_chkbx.TabIndex = 6;
            this.unmask_chkbx.Text = "Unmask";
            this.unmask_chkbx.UseVisualStyleBackColor = true;
            this.unmask_chkbx.CheckedChanged += new System.EventHandler(this.unmask_chkbx_CheckedChanged);
            // 
            // LoginForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(625, 361);
            this.Controls.Add(this.unmask_chkbx);
            this.Controls.Add(this.login_btn);
            this.Controls.Add(this.header_lbl);
            this.Controls.Add(this.txt_password);
            this.Controls.Add(this.txt_username);
            this.Controls.Add(this.passwprd_lbl);
            this.Controls.Add(this.username_lbl);
            this.Name = "LoginForm";
            this.Text = "LoginForm";
            this.Load += new System.EventHandler(this.LoginForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label username_lbl;
        private System.Windows.Forms.Label passwprd_lbl;
        private System.Windows.Forms.TextBox txt_username;
        private System.Windows.Forms.TextBox txt_password;
        private System.Windows.Forms.Label header_lbl;
        private System.Windows.Forms.Button login_btn;
        private System.Windows.Forms.CheckBox unmask_chkbx;
    }
}