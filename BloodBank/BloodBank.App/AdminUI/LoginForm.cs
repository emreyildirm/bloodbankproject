﻿using BloodBank.BLL.DTO;
using BloodBank.BLL.IServices;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace BloodBank.App.AdminUI
{
    public partial class LoginForm : Form
    {
        private readonly IAdminService adminService;
        private readonly IPendingDonorService pendingDonorService;
        private readonly IDonorService donorService;
        private readonly IBankOfBloodService bankOfBloodService;
        private List<BloodTypeDto> bloodTypes;
        public LoginForm(IAdminService adminService, IPendingDonorService pendingDonorService, IDonorService donorService, IBankOfBloodService bankOfBloodService, List<BloodTypeDto> bloodTypes)
        {
            this.adminService = adminService;
            this.bankOfBloodService = bankOfBloodService;
            this.pendingDonorService = pendingDonorService;
            this.donorService = donorService;
            InitializeComponent();
        }

        private void unmask_chkbx_CheckedChanged(object sender, EventArgs e)
        {
            txt_password.PasswordChar = unmask_chkbx.Checked ? '\0' : '*';
        }

        private void login_btn_Click(object sender, EventArgs e)
        {
            var admin = adminService.Login(txt_username.Text, txt_password.Text);
            if (admin == null)
            {
                MessageBox.Show("İnvalid Username or Password");
            }
            else
            {
                AdminPanelForm adminPanelForm = new AdminPanelForm(admin, adminService, bankOfBloodService, donorService, pendingDonorService, bloodTypes);
                adminPanelForm.Show();
                this.Close();
            }
        }

        private void LoginForm_Load(object sender, EventArgs e)
        {
            txt_password.PasswordChar = '*';
        }
    }
}
