﻿namespace BloodBank.App.AdminUI
{
    partial class AdminPanelForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView_Donors = new System.Windows.Forms.DataGridView();
            this.dataGridView_PendingDonors = new System.Windows.Forms.DataGridView();
            this.dataGridView_BloodBank = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.username_lbl = new System.Windows.Forms.Label();
            this.logout_btn = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btn_confirm = new System.Windows.Forms.Button();
            this.btn_reject = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Donors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_PendingDonors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_BloodBank)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView_Donors
            // 
            this.dataGridView_Donors.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_Donors.Location = new System.Drawing.Point(720, 130);
            this.dataGridView_Donors.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.dataGridView_Donors.Name = "dataGridView_Donors";
            this.dataGridView_Donors.RowHeadersWidth = 51;
            this.dataGridView_Donors.Size = new System.Drawing.Size(700, 450);
            this.dataGridView_Donors.TabIndex = 0;
            // 
            // dataGridView_PendingDonors
            // 
            this.dataGridView_PendingDonors.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_PendingDonors.Location = new System.Drawing.Point(10, 130);
            this.dataGridView_PendingDonors.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.dataGridView_PendingDonors.Name = "dataGridView_PendingDonors";
            this.dataGridView_PendingDonors.RowHeadersWidth = 51;
            this.dataGridView_PendingDonors.Size = new System.Drawing.Size(700, 450);
            this.dataGridView_PendingDonors.TabIndex = 0;
            // 
            // dataGridView_BloodBank
            // 
            this.dataGridView_BloodBank.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_BloodBank.Location = new System.Drawing.Point(1440, 130);
            this.dataGridView_BloodBank.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.dataGridView_BloodBank.Name = "dataGridView_BloodBank";
            this.dataGridView_BloodBank.RowHeadersWidth = 51;
            this.dataGridView_BloodBank.Size = new System.Drawing.Size(480, 450);
            this.dataGridView_BloodBank.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(868, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(184, 41);
            this.label1.TabIndex = 0;
            this.label1.Text = "Admin Panel";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(25, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "User";
            // 
            // username_lbl
            // 
            this.username_lbl.AutoSize = true;
            this.username_lbl.Location = new System.Drawing.Point(25, 42);
            this.username_lbl.Name = "username_lbl";
            this.username_lbl.Size = new System.Drawing.Size(0, 20);
            this.username_lbl.TabIndex = 2;
            // 
            // logout_btn
            // 
            this.logout_btn.Location = new System.Drawing.Point(1805, 21);
            this.logout_btn.Name = "logout_btn";
            this.logout_btn.Size = new System.Drawing.Size(94, 29);
            this.logout_btn.TabIndex = 5;
            this.logout_btn.Text = "Log out";
            this.logout_btn.UseVisualStyleBackColor = true;
            this.logout_btn.Click += new System.EventHandler(this.logout_btn_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(287, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(114, 20);
            this.label3.TabIndex = 6;
            this.label3.Text = "Pending Donors";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(1039, 82);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 20);
            this.label4.TabIndex = 7;
            this.label4.Text = "Donors";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(1625, 82);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(85, 20);
            this.label5.TabIndex = 8;
            this.label5.Text = "Blood Bank";
            // 
            // btn_confirm
            // 
            this.btn_confirm.Location = new System.Drawing.Point(187, 611);
            this.btn_confirm.Name = "btn_confirm";
            this.btn_confirm.Size = new System.Drawing.Size(94, 29);
            this.btn_confirm.TabIndex = 9;
            this.btn_confirm.Text = "Confirm";
            this.btn_confirm.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn_confirm.UseVisualStyleBackColor = true;
            this.btn_confirm.Click += new System.EventHandler(this.btn_confirm_Click);
            // 
            // btn_reject
            // 
            this.btn_reject.Location = new System.Drawing.Point(306, 611);
            this.btn_reject.Name = "btn_reject";
            this.btn_reject.Size = new System.Drawing.Size(94, 29);
            this.btn_reject.TabIndex = 10;
            this.btn_reject.Text = "Reject";
            this.btn_reject.UseVisualStyleBackColor = true;
            // 
            // AdminPanelForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1924, 750);
            this.Controls.Add(this.btn_reject);
            this.Controls.Add(this.btn_confirm);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.logout_btn);
            this.Controls.Add(this.username_lbl);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridView_Donors);
            this.Controls.Add(this.dataGridView_PendingDonors);
            this.Controls.Add(this.dataGridView_BloodBank);
            this.Name = "AdminPanelForm";
            this.Text = "AdminPanelForm";
            this.Load += new System.EventHandler(this.AdminPanelForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Donors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_PendingDonors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_BloodBank)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label username_lbl;
        private System.Windows.Forms.Button logout_btn;
        private System.Windows.Forms.DataGridView dataGridView_Donors;
        private System.Windows.Forms.DataGridView dataGridView_PendingDonors;
        private System.Windows.Forms.DataGridView dataGridView_BloodBank;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btn_confirm;
        private System.Windows.Forms.Button btn_reject;
    }
}