﻿using BloodBank.BLL.IServices;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace BloodBank.App.BankServiceUI
{
    public partial class BloodBankStatusForm : Form
    {
        private readonly IBankOfBloodService bankOfBloodService;
        public BloodBankStatusForm(IBankOfBloodService bankOfBloodService)
        {
            this.bankOfBloodService = bankOfBloodService;
            InitializeComponent();
        }
    }
}
