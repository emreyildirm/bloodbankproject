﻿using AutoMapper;
using BloodBank.BLL.DTO;
using BloodBank.DAL.Entities;

namespace BloodBank.BLL.Helper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<BloodTypeDto, BloodType>();
            CreateMap<BloodType, BloodTypeDto>();

            CreateMap<BankOfBlood, BankOfBloodDto>();
            CreateMap<BankOfBloodDto, BankOfBlood>();

            CreateMap<Donor, DonorDto>();
            CreateMap<DonorDto, Donor>();

            CreateMap<PendingDonor, PendingDonorDto>();
            CreateMap<PendingDonorDto, PendingDonor>();

            CreateMap<Donor, PendingDonorDto>();
            CreateMap<PendingDonorDto, Donor>();

            CreateMap<Admin, AdminDto>();
            CreateMap<AdminDto, Admin>();

            CreateMap<Role, RoleDto>();
            CreateMap<RoleDto, Role>();

        }
    }
}
