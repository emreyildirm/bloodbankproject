﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.BLL.DTO
{
    public class AdminDto
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int RoleId { get; set; }
        public string RoleName { get; set; }
    }
}
