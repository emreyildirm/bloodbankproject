﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.BLL.DTO
{
    public class PendingDonorDto
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Adress { get; set; }
        public string IdendityNumber { get; set; }
        public int Age { get { return (DateTime.Now - DateOfBirth).Days / 365; } }
        public DateTime DateOfBirth { get; set; }
        public int BloodQuantity { get; set; }
        public int BloodTypeId { get; set; }
        public string BloodTypeName { get; set; }
    }
}
