﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.BLL.DTO
{
    public class BankOfBloodDto
    {
        public int Id { get; set; }
        public decimal Quantity { get; set; }
        public int? BloodTypeId { get; set; }
        public string BloodTypeName { get; set; }
    }
}
