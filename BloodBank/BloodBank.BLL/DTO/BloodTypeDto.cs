﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.BLL.DTO
{
    public class BloodTypeDto
    {
        public int Id { get; set; }
        public string BloodTypeName { get; set; }
    }
}
