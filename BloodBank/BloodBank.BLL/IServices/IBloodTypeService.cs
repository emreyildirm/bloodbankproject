﻿using BloodBank.BLL.DTO;

namespace BloodBank.BLL.IServices
{
    public interface IBloodTypeService :IService<BloodTypeDto>
    {
        void Add(BloodTypeDto item);
        BloodTypeDto GetById(int id);
    }
}
