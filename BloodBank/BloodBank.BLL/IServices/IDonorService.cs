﻿using BloodBank.BLL.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.BLL.IServices
{
    public interface IDonorService : IService<DonorDto>
    {
        void ConfirmPendingDonor(PendingDonorDto pendingDonorDto);
        void Update(DonorDto item);
        void Remove(DonorDto item);
        int GetDonorCount();
        DonorDto GetById(int id);
        string GetDonorsMostlyBloodType();
        int IdGenerator();
        List<DonorDto> FindDonorWithBloodType(string bloodType);
        List<DonorDto> FindDonorWithFullName(string firstName, string lastName);
        List<DonorDto> FindDonorWithParams(string bloodType, string firstName, string lastName);
        List<DonorDto> FindDonorWithIdendityNumber(string idendityNumber);

    }
}
