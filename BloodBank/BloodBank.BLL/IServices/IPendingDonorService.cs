﻿using BloodBank.BLL.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.BLL.IServices
{
    public interface IPendingDonorService : IService<PendingDonorDto>
    {
        void Add(PendingDonorDto item);
        void Update(PendingDonorDto item);
        void Remove(PendingDonorDto item);
        int GetPendingDonorCount();
        PendingDonorDto GetById(int id);
        int IdGenerator();
        List<PendingDonorDto> FindWithBloodType(string bloodType);
        List<PendingDonorDto> FindWithFullName(string firstName, string lastName);
        List<PendingDonorDto> FindWithParams(string bloodType, string firstName, string lastName);
        List<PendingDonorDto> FindWithIdendityNumber(string idendityNumber);
    }
}
