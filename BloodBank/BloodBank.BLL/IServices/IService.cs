﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.BLL.IServices
{
    public interface IService<T>
    {
        List<T> GetAll();
        void Save();
    }
}
