﻿using BloodBank.BLL.DTO;

namespace BloodBank.BLL.IServices
{
    public interface IBankOfBloodService: IService<BankOfBloodDto>
    {
        void Add(BankOfBloodDto item);
        BankOfBloodDto GetById(int id);
        BankOfBloodDto GetByBoodType(string bloodType);
        void AddingBlood(BankOfBloodDto item);
        void BloodRemoval(BankOfBloodDto item, decimal quantity);
        void Remove(BankOfBloodDto item);
        decimal GetMaxQuantityByBloodType();
        decimal GetMinQuantityByBloodType();
        decimal GetQuantityByBloodType(int bloodTypeId);
    }
}
