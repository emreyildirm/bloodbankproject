﻿using BloodBank.BLL.DTO;

namespace BloodBank.BLL.IServices
{
    public interface IAdminService :IService<AdminDto>
    {
        void Add(AdminDto item);
        AdminDto GetById(int id);
        AdminDto Login(string username, string password);
    }
}
