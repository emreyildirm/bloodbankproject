﻿using BloodBank.BLL.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.BLL.IServices
{
    public interface IRoleService : IService<RoleDto>
    {
        void Add(RoleDto item);
        RoleDto GetById(int id);
    }
}
