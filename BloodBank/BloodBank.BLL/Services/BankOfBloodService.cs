﻿using AutoMapper;
using BloodBank.BLL.DTO;
using BloodBank.BLL.IServices;
using BloodBank.DAL.Entities;
using BloodBank.DAL.Repositories.IRepo;
using BloodBank.DAL.UnitOfWork;
using System.Collections.Generic;
using System.Linq;

namespace BloodBank.BLL.Services
{
    public class BankOfBloodService : IBankOfBloodService
    {
        private readonly IMapper mapper;
        private readonly IUnitOfWork unitOfWork;
        private readonly IBankOfBloodRepository bankOfBloodRepository;
        public BankOfBloodService(IBankOfBloodRepository bankOfBloodRepository, IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.mapper = mapper;
            this.bankOfBloodRepository = bankOfBloodRepository;
            this.unitOfWork = unitOfWork;
        }
        public void Add(BankOfBloodDto item)
        {
            var bankOfBlood = mapper.Map<BankOfBlood>(item);
            bankOfBloodRepository.Add(bankOfBlood);
            Save();
        }

        public void AddingBlood(BankOfBloodDto item)
        {
            var source = bankOfBloodRepository.GetById(item.Id);
            BankOfBlood bankOfBlood = mapper.Map<BankOfBlood>(source);
            bankOfBlood.Quantity += 2;
            Save();
        }

        public List<BankOfBloodDto> GetAll()
        {
            var bloodBankListPoco = bankOfBloodRepository.GetAll();
            List<BankOfBloodDto> bloodBankList = new List<BankOfBloodDto>();
            foreach (object item in bloodBankListPoco)
            {
                var bankOfBloodDto = mapper.Map<BankOfBloodDto>(item);
                bloodBankList.Add(bankOfBloodDto);
            }
            return bloodBankList;
        }

        public BankOfBloodDto GetById(int id)
        {
            var source = bankOfBloodRepository.GetById(id);
            return mapper.Map<BankOfBloodDto>(source);
        }

        public decimal GetQuantityByBloodType(int bloodTypeId)
        {
            var bloodBankList = GetAll();
            if (bloodBankList.Count == 0)
                return 0;
            else
            {
                var bloodQuantity = bloodBankList.FirstOrDefault(x => x.BloodTypeId == bloodTypeId).Quantity;
                return bloodQuantity;
            }
        }        
        public decimal GetMaxQuantityByBloodType()
        {
            var bloodBankList = GetAll();
            if (bloodBankList.Count == 0)
                return 0;
            else
            {
                var maxBloodQuantity = bloodBankList.Max(x => x.Quantity);
                return 0;
            }
            
        }        
        public decimal GetMinQuantityByBloodType()
        {
            var bloodBankList = GetAll();
            if (bloodBankList.Count == 0)
                return 0;
            else 
            {
                var minBloodQuantity = bloodBankList.Min(x => x.Quantity);
                return minBloodQuantity;
            }
        }

        public void Remove(BankOfBloodDto item)
        {
            var source = mapper.Map<BankOfBlood>(item);
            bankOfBloodRepository.Delete(source);
            Save();
        }

        public void Save()
        {
            unitOfWork.Save();
        }

        public BankOfBloodDto GetByBoodType(string bloodType)
        {
            var source = GetAll().FirstOrDefault(x => x.BloodTypeName == bloodType);
            var bankDto = mapper.Map<BankOfBloodDto>(source);
            return bankDto;
        }
        //TODO:
        public void BloodRemoval(BankOfBloodDto item, decimal quantity)
        {
            throw new System.NotImplementedException();
        }
    }
}
