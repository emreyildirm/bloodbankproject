﻿using AutoMapper;
using BloodBank.BLL.DTO;
using BloodBank.BLL.IServices;
using BloodBank.DAL.Entities;
using BloodBank.DAL.Repositories.IRepo;
using BloodBank.DAL.UnitOfWork;
using System.Collections.Generic;
using System.Linq;

namespace BloodBank.BLL.Services
{
    public class DonorService : IDonorService
    {
        private readonly IMapper mapper;
        private readonly IDonorRepository donorRepository;
        private readonly IUnitOfWork unitOfWork;
        public DonorService(IDonorRepository donorRepository, IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.donorRepository = donorRepository;
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }
        public void ConfirmPendingDonor(PendingDonorDto item)
        {
            Donor donor = mapper.Map<Donor>(item);
            donorRepository.Add(donor);
            Save();
        }


        public void Update(DonorDto item)
        {
            var source = donorRepository.GetById(item.Id);
            Donor donor = mapper.Map<Donor>(item);
            mapper.Map<Donor, Donor>(donor, source);
            Save();
        }

        public List<DonorDto> GetAll()
        {
            var donorListPoco = donorRepository.GetAll();
            List<DonorDto> donorList = new List<DonorDto>();
            foreach(Donor donor in donorListPoco)
            {
                var donorDto = mapper.Map<DonorDto>(donor);
                donorList.Add(donorDto);
            }
            return donorList;
        }

        public DonorDto GetById(int id)
        {
            var donor = donorRepository.GetById(id);
            var donorDto = mapper.Map<DonorDto>(donor);
            return donorDto;
        }

        public void Remove(DonorDto item)
        {
            var donor = donorRepository.GetById(item.Id);
            donorRepository.Delete(donor);
            Save();
        }
        
        public int GetDonorCount()
        {
            var donorList = GetAll();
            return donorList.Count;
        }
        public string GetDonorsMostlyBloodType()
        {
            return GetAll().GroupBy(x => x.BloodTypeName).Max().First().BloodTypeName;
        }
        public void Save()
        {
            unitOfWork.Save();
        }

        public int IdGenerator()
        {
            var donorCount = GetAll().Count;
            return donorCount + 1;
        }

        public List<DonorDto> FindDonorWithBloodType(string bloodType)
        {
            return GetAll().FindAll(x=> x.BloodTypeName == bloodType);
        }

        public List<DonorDto> FindDonorWithFullName(string firstName, string lastName)
        {
            return GetAll().FindAll(x => x.FirstName == firstName && x.LastName == lastName);
        }

        public List<DonorDto> FindDonorWithParams(string bloodType, string firstName, string lastName)
        {
            throw new System.NotImplementedException();
        }

        public List<DonorDto> FindDonorWithIdendityNumber(string idendityNumber)
        {
            return GetAll().FindAll(x => x.IdendityNumber == idendityNumber);
        }
    }
}
