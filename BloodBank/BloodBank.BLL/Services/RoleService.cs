﻿using AutoMapper;
using BloodBank.BLL.DTO;
using BloodBank.BLL.IServices;
using BloodBank.DAL.Entities;
using BloodBank.DAL.Repositories.IRepo;
using BloodBank.DAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.BLL.Services
{
    public class RoleService : IRoleService
    {
        private readonly IRoleRepository roleRepository;
        private readonly IMapper mapper;
        private readonly IUnitOfWork unitOfWork;
        public RoleService(IRoleRepository roleRepository, IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.roleRepository = roleRepository;
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }
        public void Add(RoleDto item)
        {
            var role = mapper.Map<Role>(item);
            roleRepository.Add(role);
            Save();
        }

        public List<RoleDto> GetAll()
        {
            var roleListPoco = roleRepository.GetAll();
            List<RoleDto> roleList = new List<RoleDto>();
            foreach(object item in roleListPoco)
            {
                var roleDto = mapper.Map<RoleDto>(item);
                roleList.Add(roleDto);
            }
            return roleList;
        }

        public RoleDto GetById(int id)
        {
            throw new NotImplementedException();
        }

        public void Save()
        {
            unitOfWork.Save();
        }
    }
}
