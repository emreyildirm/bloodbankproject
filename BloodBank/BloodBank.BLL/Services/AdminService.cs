﻿using AutoMapper;
using BloodBank.BLL.DTO;
using BloodBank.BLL.IServices;
using BloodBank.DAL.Entities;
using BloodBank.DAL.Repositories.IRepo;
using BloodBank.DAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BloodBank.BLL.Services
{
    public class AdminService : IAdminService
    {
        private readonly IAdminRepository adminRepository;
        private readonly IMapper mapper;
        private readonly IUnitOfWork unitOfWork;
        public AdminService(IAdminRepository adminRepository, IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.adminRepository = adminRepository;
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }
        public void Add(AdminDto item)
        {
            var role = mapper.Map<Admin>(item);
            adminRepository.Add(role);
            Save();
        }

        public List<AdminDto> GetAll()
        {
            var adminListPoco = adminRepository.GetAll();
            List<AdminDto> adminList = new List<AdminDto>();
            foreach (object item in adminListPoco)
            {
                var adminDto = mapper.Map<AdminDto>(item);
                adminList.Add(adminDto);
            }
            return adminList;
        }

        public AdminDto GetById(int id)
        {
            var admin = adminRepository.GetById(id);
            var adminDto = mapper.Map<AdminDto>(admin);
            return adminDto;
        }

        public AdminDto Login(string username, string password)
        {
            var adminList = adminRepository.GetAll();
            var admin = adminList.FirstOrDefault(x => x.Username == username && x.Password == password);
            if (admin == null)
                return null;
            else
               return mapper.Map<AdminDto>(admin);
        }

        public void Save()
        {
            unitOfWork.Save();
        }
    }
}
