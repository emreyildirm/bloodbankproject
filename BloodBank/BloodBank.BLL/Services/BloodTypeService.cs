﻿using AutoMapper;
using BloodBank.BLL.DTO;
using BloodBank.BLL.IServices;
using BloodBank.DAL.Entities;
using BloodBank.DAL.Repositories.IRepo;
using BloodBank.DAL.UnitOfWork;
using System.Collections.Generic;

namespace BloodBank.BLL.Services
{
    public delegate List<object> BloodTypeHandler();
    public class BloodTypeService : IBloodTypeService
    {
        private readonly IMapper mapper;
        private readonly IBloodTypeRepository bloodTypeRepository;
        private readonly IUnitOfWork unitOfWork;
        public BloodTypeService(IMapper mapper, IBloodTypeRepository bloodTypeRepository, IUnitOfWork unitOfWork)
        {
            this.mapper = mapper;
            this.unitOfWork = unitOfWork;
            this.bloodTypeRepository = bloodTypeRepository;
        }
        public void Add(BloodTypeDto item)
        {
            var bloodType = mapper.Map<BloodType>(item);
            bloodTypeRepository.Add(bloodType);
            Save();
        }

        public List<BloodTypeDto> GetAll()
        {
            var bloodTypeList = bloodTypeRepository.GetAll();
            List<BloodTypeDto> donorList = new List<BloodTypeDto>();
            foreach (object item in bloodTypeList)
            {
                var bloodTypeDto = mapper.Map<BloodTypeDto>(item);
                donorList.Add(bloodTypeDto);
            }
            return donorList;
        }

        public BloodTypeDto GetById(int id)
        {
            var bloodType = bloodTypeRepository.GetById(id);
            return mapper.Map<BloodTypeDto>(bloodType);
        }

        public void Save()
        {
            unitOfWork.Save();
        }
    }
}