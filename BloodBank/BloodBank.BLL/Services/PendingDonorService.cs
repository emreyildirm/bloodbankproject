﻿using AutoMapper;
using BloodBank.BLL.DTO;
using BloodBank.BLL.IServices;
using BloodBank.DAL.Entities;
using BloodBank.DAL.Repositories.IRepo;
using BloodBank.DAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.BLL.Services
{
    public class PendingDonorService : IPendingDonorService
    {
        private readonly IPendingDonorRepository pendingDonorRepository;
        private readonly IMapper mapper;
        private readonly IUnitOfWork unitOfWork;
        public PendingDonorService(IPendingDonorRepository pendingDonorRepository, IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.pendingDonorRepository = pendingDonorRepository;
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }
        public void Add(PendingDonorDto item)
        {
            var pendingDonor = mapper.Map<PendingDonor>(item);
            pendingDonorRepository.Add(pendingDonor);
            Save();
        }

        public List<PendingDonorDto> FindWithBloodType(string bloodType)
        {
            return GetAll().FindAll(x => x.BloodTypeName == bloodType);
        }

        public List<PendingDonorDto> FindWithFullName(string firstName, string lastName)
        {
            return GetAll().FindAll(x => x.FirstName == firstName && x.LastName == lastName);
        }

        public List<PendingDonorDto> FindWithIdendityNumber(string idendityNumber)
        {
            return GetAll().FindAll(x => x.IdendityNumber == idendityNumber);
        }

        public List<PendingDonorDto> FindWithParams(string bloodType, string firstName, string lastName)
        {
            throw new NotImplementedException();
        }

        public List<PendingDonorDto> GetAll()
        {
            var ListPoco = pendingDonorRepository.GetAll();
            List<PendingDonorDto> donorList = new List<PendingDonorDto>();
            foreach (PendingDonor donor in ListPoco)
            {
                var pendingDto = mapper.Map<PendingDonorDto>(donor);
                donorList.Add(pendingDto);
            }
            return donorList;
        }

        public PendingDonorDto GetById(int id)
        {
            var pendingDonor = pendingDonorRepository.GetById(id);
            var pendingDonorDto = mapper.Map<PendingDonorDto>(pendingDonor);
            return pendingDonorDto;
        }

        public int GetPendingDonorCount()
        {
            var pendingDonorList = GetAll();
            return pendingDonorList.Count;
        }

        public int IdGenerator()
        {
            var donorCount = GetAll().Count;
            return donorCount + 1;
        }

        public void Remove(PendingDonorDto item)
        {
            var pendingDonor = pendingDonorRepository.GetById(item.Id);
            pendingDonorRepository.Delete(pendingDonor);
            Save();
        }

        public void Save()
        {
            unitOfWork.Save();
        }

        public void Update(PendingDonorDto item)
        {
            var source = pendingDonorRepository.GetById(item.Id);
            PendingDonor donor = mapper.Map<PendingDonor>(item);
            mapper.Map<PendingDonor, PendingDonor>(donor, source);
            Save();
        }
    }
}
