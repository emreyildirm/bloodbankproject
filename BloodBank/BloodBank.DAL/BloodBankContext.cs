﻿using BloodBank.DAL.Entities;
using BloodBank.DAL.EntitiesMappings;
using Microsoft.EntityFrameworkCore;

namespace BloodBank.DAL
{
    public class BloodBankContext : DbContext
    {
        public BloodBankContext(DbContextOptions<BloodBankContext> options) : base(options)
        {


        }
        public BloodBankContext()
        {


        }
        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) => optionsBuilder.UseSqlServer("BloodBankDb");

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //if (!optionsBuilder.IsConfigured)
            //{
            //    var connString = "Server=KAMOHOBE\\SQLEXPRESS;Database=BloodBankDB;Trusted_Connection=True";
            //    optionsBuilder
            //        .UseSqlServer(connString, providerOptions => providerOptions.CommandTimeout(60))
            //        .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            //}
            optionsBuilder.UseSqlServer("Server = KAMOHOBE\\SQLEXPRESS; Database = BloodBankDB; Trusted_Connection = True");
            base.OnConfiguring(optionsBuilder);
        }

        public DbSet<BankOfBlood> BloodBank { get; set; }
        public DbSet<BloodType> BloodTypes { get; set; }
        public DbSet<Donor> Donors { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Admin> Admins { get; set; }
        public DbSet<PendingDonor> PendingDonors { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new DonorMap());
            modelBuilder.ApplyConfiguration(new BankOfBloodMap());
            modelBuilder.ApplyConfiguration(new BloodTypeMap());
            modelBuilder.ApplyConfiguration(new AdminMap());
            modelBuilder.ApplyConfiguration(new RoleMap());
            modelBuilder.ApplyConfiguration(new PendingDonorMap());
        }
    }
}
