﻿using BloodBank.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BloodBank.DAL.EntitiesMappings
{
    public class DonorMap : IEntityTypeConfiguration<Donor>
    {
        public void Configure(EntityTypeBuilder<Donor> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id)
                .ValueGeneratedNever()
                .HasColumnName("Id");

            builder.Property(d => d.FirstName)
                .HasMaxLength(40)
                .HasColumnName("FirstName");

            builder.Property(d => d.LastName)
                .HasMaxLength(40)
                .HasColumnName("LastName");

            builder.Property(d => d.Adress)
                .HasMaxLength(500)
                .HasColumnName("Adress");

            builder.Property(d => d.PhoneNumber)
                .HasMaxLength(10)
                .HasColumnName("PhoneNumber");

            builder.Property(d => d.Age)
                .HasColumnName("Age");

            builder.Property(d => d.BloodTypeId)
                 .HasColumnName("BloodTypeId");

            builder.Property(d => d.BloodTypeName)
                .HasColumnName("BloodTypeName");



            builder
                .HasOne<BloodType>(d => d.BloodType)
                .WithMany(b => b.Donors)
                .HasForeignKey(d => d.BloodTypeId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.ToTable("Donors");
        }
    }
}
