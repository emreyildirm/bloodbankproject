﻿using BloodBank.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.DAL.EntitiesMappings
{
    public class AdminMap : IEntityTypeConfiguration<Admin>
    {
        public void Configure(EntityTypeBuilder<Admin> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id)
                .ValueGeneratedNever()
                .HasColumnName("Id");

            builder.Property(d => d.Username)
                .HasMaxLength(40)
                .HasColumnName("UserName");

            builder.Property(d => d.Password)
                .HasMaxLength(40)
                .HasColumnName("Password");

            //builder.Property(d => d.RoleId)
            //     .HasColumnName("RoleId");

            //builder.Property(d => d.RoleName)
            //    .HasColumnName("RoleName");

            //builder
            //    .HasOne<Role>(d => d.Role)
            //    .WithMany(b => b.Admins)
            //    .HasForeignKey(d => d.RoleId)
            //    .OnDelete(DeleteBehavior.Cascade);

            builder.ToTable("Admins");
        }
    }
}
