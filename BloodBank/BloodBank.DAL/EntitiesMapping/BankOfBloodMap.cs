﻿using BloodBank.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.DAL.EntitiesMappings
{
    public class BankOfBloodMap : IEntityTypeConfiguration<BankOfBlood>
    {
        public void Configure(EntityTypeBuilder<BankOfBlood> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id)
                .ValueGeneratedNever()
                .HasColumnName("Id");

            builder.Property(x => x.Quantity)
                .HasColumnName("Quantity")
                .HasDefaultValue(0);

            builder
                .HasOne<BloodType>(x => x.BloodType)
                .WithOne(b => b.Bank)
                .OnDelete(DeleteBehavior.Cascade);

            builder.ToTable("BloodBank");
        }
    }
}
