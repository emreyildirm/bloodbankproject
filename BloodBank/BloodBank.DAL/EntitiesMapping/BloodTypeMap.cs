﻿using BloodBank.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BloodBank.DAL.EntitiesMappings
{
    public class BloodTypeMap : IEntityTypeConfiguration<BloodType>
    {
        public void Configure(EntityTypeBuilder<BloodType> builder)
        {
            builder
                .HasKey(x => x.Id);

            builder
                .Property(x => x.Id)
                .ValueGeneratedNever()
                .HasColumnName("Id");

            builder.
                Property(x => x.BloodTypeName)
                .HasColumnName("BloodType");

            builder
                .HasOne<BankOfBlood>(x => x.Bank)
                .WithOne(x => x.BloodType)
                .HasForeignKey<BankOfBlood>(x => x.BloodTypeId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.ToTable("BloodTypes");
        }
    }
}
