﻿using BloodBank.DAL.Entities;
using BloodBank.DAL.Repositories.IRepo;

namespace BloodBank.DAL.Repositories.Repo
{
    public class PendingDonorRepository : BaseRepository<PendingDonor> , IPendingDonorRepository
    {
        public PendingDonorRepository(BloodBankContext dbContext) : base(dbContext)
        {
        }
    }
}
