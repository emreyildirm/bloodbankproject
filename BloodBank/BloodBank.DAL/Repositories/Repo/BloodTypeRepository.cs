﻿using BloodBank.DAL.Entities;
using BloodBank.DAL.Repositories.IRepo;
using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.DAL.Repositories.Repo
{
    public class BloodTypeRepository : BaseRepository<BloodType>, IBloodTypeRepository
    {
        public BloodTypeRepository(BloodBankContext dbContext) : base(dbContext)
        {
        }
    }
}
