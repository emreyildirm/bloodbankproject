﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace BloodBank.DAL.Repositories.Repo
{
    public abstract class BaseRepository<T> where T : class
    {
        private  BloodBankContext _context;
        protected BloodBankContext context { get { return _context; } }
        protected BaseRepository(BloodBankContext dbContext)
        {
            if (dbContext == null)
                throw new ArgumentNullException("dbContext can not be null.");
            _context = dbContext;
        }
        public virtual void Add(T Entity)
        {
            _context.Add<T>(Entity);
        }

        public virtual void Delete(T Entity)
        {
            _context.Set<T>().Remove(Entity);
        }

        public virtual List<T> GetAll(Expression<Func<T, bool>> Filter = null)
        {
            return _context.Set<T>().ToList();
        }

        public virtual T GetById(object EntityId)
        {
            return _context.Set<T>().Find(EntityId);
        }

        public virtual void Update(T Entity)
        {
            var updatedEntity = _context.Entry(Entity);
            updatedEntity.State = EntityState.Modified;
        }
        //private PropertyInfo GetIdPropInfo()
        //{
        //    var type = typeof(T);
        //    var result = "Id";
        //    return type.GetProperty(result);
        //}
    }
}
