﻿using BloodBank.DAL.Entities;
using BloodBank.DAL.Repositories.IRepo;
using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.DAL.Repositories.Repo
{
    public class AdminRepository : BaseRepository<Admin>, IAdminRepository
    {
        public AdminRepository(BloodBankContext dbContext) : base(dbContext)
        {
        }
    }
}
