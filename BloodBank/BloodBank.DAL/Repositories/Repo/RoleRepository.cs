﻿using BloodBank.DAL.Entities;
using BloodBank.DAL.Repositories.IRepo;
using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.DAL.Repositories.Repo
{
    public class RoleRepository : BaseRepository<Role>, IRoleRepository
    {
        public RoleRepository(BloodBankContext dbContext) : base(dbContext)
        {
        }
    }
}
