﻿using BloodBank.DAL.Entities;
using BloodBank.DAL.Repositories.IRepo;
using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.DAL.Repositories.Repo
{
    public class DonorRepository : BaseRepository<Donor>, IDonorRepository
    {
        public DonorRepository(BloodBankContext dbContext) : base(dbContext)
        {
        }
    }
}
