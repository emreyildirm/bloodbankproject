﻿using BloodBank.DAL.Repositories.IRepo;
using System;
using System.Collections.Generic;
using System.Text;
using BloodBank.DAL.Entities;

namespace BloodBank.DAL.Repositories.Repo
{
    public class BankOfBloodRepository : BaseRepository<BankOfBlood>, IBankOfBloodRepository
    {
        public BankOfBloodRepository(BloodBankContext Context) : base(Context)
        {

        }
    }
}
