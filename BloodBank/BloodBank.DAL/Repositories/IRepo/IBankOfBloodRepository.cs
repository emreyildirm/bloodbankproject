﻿using BloodBank.DAL.Entities;

namespace BloodBank.DAL.Repositories.IRepo
{
    public interface IBankOfBloodRepository : IBaseRepository<BankOfBlood>
    {
    }
}
