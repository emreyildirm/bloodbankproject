﻿using BloodBank.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.DAL.Repositories.IRepo
{
    public interface IRoleRepository : IBaseRepository<Role>
    {
    }
}
