﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.DAL.Entities
{
    public class BloodType
    {
        public int Id { get; set; }
        public string BloodTypeName { get; set; }
        public virtual ICollection<Donor> Donors { get; set; }
        public virtual ICollection<PendingDonor> PendingDonors { get; set; }
        public virtual BankOfBlood Bank { get; set; }

    }
}
