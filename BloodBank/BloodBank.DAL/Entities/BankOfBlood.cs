﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.DAL.Entities
{
    public class BankOfBlood
    {
        public int Id { get; set; }
        public decimal Quantity { get; set; }
        public int? BloodTypeId { get; set; }
        public string BloodTypeName { get; set; }
        public virtual BloodType BloodType { get; set; }
    }
}
