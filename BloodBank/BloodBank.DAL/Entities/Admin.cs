﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.DAL.Entities
{
    public class Admin
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        //public Role Role { get; set; }
        //public int RoleId { get; set; }
        //public string RoleName { get; set; }
    }
}
