﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.DAL.Entities
{
    public class PendingDonor
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Adress { get; set; }
        public string PhoneNumber { get; set; }
        public int Age { get; set; }
        //public DateTime DateOfBirth { get; set; }
        public int? BloodTypeId { get; set; }
        public string BloodTypeName { get; set; }
        public virtual BloodType BloodType { get; set; }
        //public int BloodQuantity { get; set; }
    }
}
