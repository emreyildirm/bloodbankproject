﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.DAL.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {

        private bool disposed = false;
        private BloodBankContext _dbContext;
        public UnitOfWork(BloodBankContext dbContext)
        {
            _dbContext = dbContext;
        }
        public BloodBankContext DbContext
        {
            get
            {
                return _dbContext;
            }
        }
        public void Save()
        {
            try
            {
                DbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _dbContext.Dispose();
                }
            }

            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
