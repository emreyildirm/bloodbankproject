﻿using System;

namespace BloodBank.DAL.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        void Save();
    }
}
